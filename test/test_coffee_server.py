import pytest
import json
from sqlalchemy import create_engine
from positive_coffee.init_db import init_db
from positive_coffee.coffee_server import app, Session


@pytest.fixture
def client():
    with open('./config.json') as config_fd:
        config = json.load(config_fd)

    app.config['TESTING'] = True
    engine = create_engine(config['TEST_DB_CONNECTION_URL'], echo=False)
    init_db(engine)
    Session.configure(bind=engine)
    return app.test_client()


@pytest.fixture
def initial_user(client):
    client.post('/users', data={'name': 'Ivan', 'email': 'ivan@mail.ru'})


def test_user_getting(client):
    resp = client.get('/users')
    assert resp.status_code == 200
    assert resp.json == {'result': []}


def test_user_addition(client):
    resp = client.post('/users', data={'name': 'Ivan', 'email': 'ivan@mail.ru'})
    assert resp.status_code == 200
    assert resp.json == {'result': {'name': 'Ivan', 'email': 'ivan@mail.ru'}}

    resp = client.get('/users')
    assert resp.status_code == 200
    assert resp.json == {'result': [{'name': 'Ivan', 'email': 'ivan@mail.ru'}]}


def test_user_getting_by_id(client):
    client.post('/users', data={'name': 'Ivan', 'email': 'ivan@mail.ru'})

    resp = client.get('/users/1')
    assert resp.status_code == 200
    assert resp.json == {'result': {'name': 'Ivan', 'email': 'ivan@mail.ru'}}

    resp = client.get('/users/2')
    assert resp.status_code == 404


def test_bad_user_addition(client):
    resp = client.post('/users')
    assert resp.status_code == 400


def test_user_addition_with_existing_email(client):
    client.post('/users', data={'name': 'Ivan', 'email': 'ivan@mail.ru'})
    resp = client.post('/users', data={'name': 'Johny', 'email': 'ivan@mail.ru'})
    assert resp.status_code == 400


def test_orders_getting(client):
    resp = client.get('/orders')
    assert resp.status_code == 200
    assert resp.json == {'pending': [], 'in_progress': [], 'finished': []}


def test_order_placing_with_empty_user(client):
    resp = client.post('/orders', data={'coffee_name': 'americano'})
    assert resp.status_code == 200
    assert resp.json == {'order_id': 1}

    resp = client.get('/orders')
    assert resp.status_code == 200
    assert resp.json == {'pending': [{'user_name': '', 'order_id': 1}], 'in_progress': [], 'finished': []}


@pytest.mark.usefixtures('initial_user')
def test_order_placing(client):
    resp = client.post('/orders', data={'user_id': 1, 'coffee_name': 'americano'})
    assert resp.status_code == 200
    assert resp.json == {'order_id': 1}

    resp = client.get('/orders')
    assert resp.status_code == 200
    assert resp.json == {'pending': [{'user_name': 'Ivan', 'order_id': 1}], 'in_progress': [], 'finished': []}


def test_empty_order_placing(client):
    resp = client.post('/orders')
    assert resp.status_code == 400


@pytest.mark.usefixtures('initial_user')
def test_order_placing_with_nonexisting_user(client):
    resp = client.post('/orders', data={'user_id': 2, 'coffee_name': 'americano'})
    assert resp.status_code == 400


@pytest.mark.usefixtures('initial_user')
def test_order_placing_with_wrong_coffee_type(client):
    resp = client.post('/orders', data={'user_id': 1, 'coffee_name': 'latte'})
    assert resp.status_code == 400
