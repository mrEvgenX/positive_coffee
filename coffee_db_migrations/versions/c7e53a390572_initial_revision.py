"""initial_revision

Revision ID: c7e53a390572
Revises: 
Create Date: 2019-06-30 10:35:31.146315

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c7e53a390572'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'coffee_types',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('name', sa.String(), nullable=False, unique=True),
        sa.Column('seconds_to_make', sa.Integer(), nullable=False)
    )
    op.create_table(
        'users',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('email', sa.String(), nullable=False, unique=True)
    )
    op.create_table(
        'orders',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('coffee_type', sa.Integer(), sa.ForeignKey('coffee_types.id'), nullable=False),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('users.id')),
        sa.Column('status', sa.Integer(), nullable=False),
        sa.Column('finishing_time', sa.DateTime())
    )
    op.execute(
        "INSERT INTO coffee_types (name, seconds_to_make) "
        "VALUES ('Americano', 10), ('Cappuccino', 20)"
    )


def downgrade():
    op.drop_table('coffee_types')
    op.drop_table('users')
    op.drop_table('orders')
