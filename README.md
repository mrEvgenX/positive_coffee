# Positive Coffee

### Инструкция по запуску

Все действия выполняются в папке с проектом.

В `config.json` указать параметры подключения к базе данных `postgresql://{user}:{password}@{host}:{port}/{database}`,
есть возможность указать как боевую, так и тестовую базу (см. "Прогнать тесты").
А также настраиваем порт, на котором будет запущено приложение.
В `alembic.ini` в секции `alembic` редактируем `sqlalchemy.url` для боевой базы.
После чего устанавливаем виртуальное окружение, устанавливаем зависимости
и инициализируем базу данных, создав все необходимые таблицы:

- `virtualenv venv`
- `./venv/bin/pip install -r requirements.txt`
- `./venv/bin/alembic upgrade head`

Для запуска выполняем:

- `./venv/bin/python3 -m positive_coffee.coffee_server`

Тестовые запросы:

- `curl -d "name=ivan&email=IvanD@mail.ru" 'http://localhost:5000/users'`
- `curl 'http://localhost:5000/users'`
- `curl -d 'coffee_name=cappuccino&user_id=1' 'http://localhost:5000/orders'`
- `curl 'http://localhost:5000/orders'`

### Прогнать тесты

При необходимости инициализируем тестовую базу

- В `alembic.ini` в секции `alembic` редактируем `sqlalchemy.url` для тестовой базы.
- `./venv/bin/alembic upgrade head`

Запускаем прогон:

- `./venv/bin/pytest`
