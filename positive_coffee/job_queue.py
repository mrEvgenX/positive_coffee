import threading
import time
from datetime import datetime
import logging
import sys

from positive_coffee.models import Order, OrderStatus


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
new_order_lock = threading.Lock()


class ServeQueue:

    def __init__(self, workers_num, session_maker):
        self.Session = session_maker
        self.worker_threads = []
        for _ in range(workers_num):
            t = threading.Thread(target=self.serve_orders)
            t.daemon = True
            t.start()
            self.worker_threads.append(t)

    def serve_orders(self):
        session = self.Session()
        try:
            while True:
                order = self.get_another_order(session)
                if order:
                    self.make_ordered_coffee(order)
                    self.mark_order_finished(order.id, session)
                time.sleep(0.1)
        finally:
            session.close()

    @staticmethod
    def make_ordered_coffee(order):
        logging.info("Preparation #%s started!", order.id)
        coffee_name = order.coffee_ordered.name
        duration = order.coffee_ordered.seconds_to_make
        for i in range(duration):
            logging.info('%s %s/%s', coffee_name.capitalize(), i + 1, duration)
            time.sleep(1)

    @staticmethod
    def mark_order_finished(order_id, session):
        order = session.query(Order).filter_by(id=order_id).first()
        if order:
            order.status = OrderStatus.FINISHED.value
            order.finishing_time = datetime.today()
            session.commit()
            logging.info("Preparation #%s finished!", order.id)

    @staticmethod
    def get_another_order(session):
        new_order_lock.acquire()
        order = session.query(Order).filter_by(status=0).first()
        if order:
            order.status = OrderStatus.IN_PROGRESS.value
            session.commit()
        new_order_lock.release()
        return order
