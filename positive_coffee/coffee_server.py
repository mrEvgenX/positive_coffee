import json
from datetime import datetime, timedelta
import logging

from flask import Flask, abort, jsonify, request
from sqlalchemy import and_, create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker
import sys
from positive_coffee.job_queue import ServeQueue
from positive_coffee.models import CoffeeType, Order, User, OrderStatus


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
Session = sessionmaker()
app = Flask(__name__)


@app.route('/users', methods=['GET'])
def all_users():
    session = Session()
    try:
        users = [
            {'name': user.name, 'email': user.email}
            for user in session.query(User).order_by(User.id)
        ]
        return jsonify(result=users)
    finally:
        session.close()


@app.route('/users/<int:id_>', methods=['GET'])
def user_by_id(id_):
    session = Session()
    try:
        user = session.query(User).filter_by(id=id_).first()
        if not user:
            abort(404, {'error': 'User with id={} does not exist.'.format(id_)})
        return jsonify(result={'name': user.name, 'email': user.email})
    finally:
        session.close()


@app.route('/users', methods=['POST'])
def add_user():
    name = request.values.get('name')
    email = request.values.get('email')
    session = Session()
    try:
        if not name or not email:
            abort(400, {'error': '"name" and "email" required.'})
        user = User(name, email.lower())
        session.add(user)
        session.commit()
        return jsonify(
            result={'name': user.name, 'email': user.email}
        )
    except IntegrityError:
        abort(400, {'error': 'User with email {} already exists.'.format(email)})
    finally:
        session.close()


@app.route('/orders', methods=['GET'])
def orders_table():
    session = Session()
    try:
        pending = [
            {
                'user_name': order.user_ordered.name if order.user_ordered else '',
                'order_id': order.id
            }
            for order in session.query(Order).filter_by(status=OrderStatus.PENDING.value).order_by(Order.id)
        ]
        in_progress = [
            {
                'user_name': order.user_ordered.name if order.user_ordered else '',
                'order_id': order.id
            }
            for order in session.query(Order).filter_by(status=OrderStatus.IN_PROGRESS.value).order_by(Order.id)
        ]
        finished = [
            {
                'user_name': order.user_ordered.name if order.user_ordered else '',
                'order_id': order.id
            }
            for order in session.query(Order).filter(
                and_(
                    Order.status == OrderStatus.FINISHED.value,
                    Order.finishing_time >= datetime.today() - timedelta(minutes=1)
                )
            ).order_by(Order.id)
        ]
        return jsonify(
            pending=pending,
            in_progress=in_progress,
            finished=finished
        )
    finally:
        session.close()


@app.route('/orders', methods=['POST'])
def place_order():
    user_id = request.values.get('user_id')
    coffee_name = request.values.get('coffee_name')
    session = Session()
    try:
        if not coffee_name:
            abort(400, {'error': '"coffee_name" required.'})
        coffee_types = {
            coffee_type.name.capitalize(): coffee_type.id
            for coffee_type in session.query(CoffeeType).order_by(CoffeeType.id)
        }
        coffee_type = coffee_types.get(coffee_name.capitalize())
        if not coffee_type:
            abort(400, {
                'error': 'Invalid "coffee_name", accepted values: {}.'.format(
                    ', '.join(coffee_types.keys())
                )
            })
        order = Order(coffee_type, user_id)
        session.add(order)
        session.commit()
        return jsonify(
            order_id=order.id
        )
    except IntegrityError:
        abort(400, {'error': 'User with id={} does not exist.'.format(user_id)})
    finally:
        session.close()


@app.errorhandler(400)
def wrong_arguments(e):
    logging.exception(e)
    return jsonify(error=e.description['error']), 400


@app.errorhandler(404)
def content_not_found(e):
    logging.exception(e)
    return jsonify(error=e.description['error']), 404


@app.errorhandler(405)
def method_not_allowed(e):
    logging.exception(e)
    return jsonify(error="Method not allowed"), 405


if __name__ == '__main__':
    with open('./config.json') as config_fd:
        config = json.load(config_fd)
    engine = create_engine(config['DB_CONNECTION_URL'], echo=False)
    Session.configure(bind=engine)
    queue = ServeQueue(config['WORKERS_NUM'], Session)
    app.run(port=config['PORT'])
