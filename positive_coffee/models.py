from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from enum import Enum


class OrderStatus(Enum):
    PENDING = 0
    IN_PROGRESS = 1
    FINISHED = 2


CoffeeModel = declarative_base()


class CoffeeType(CoffeeModel):
    __tablename__ = 'coffee_types'
    id = Column(Integer(), primary_key=True)
    name = Column(String(), nullable=False, unique=True)
    seconds_to_make = Column(Integer(), nullable=False)

    def __init__(self, name, seconds_to_make):
        self.name = name
        self.seconds_to_make = seconds_to_make


class User(CoffeeModel):
    __tablename__ = 'users'
    id = Column(Integer(), primary_key=True)
    name = Column(String(), nullable=False)
    email = Column(String(), nullable=False, unique=True)

    def __init__(self, name, email):
        self.name = name
        self.email = email


class Order(CoffeeModel):
    __tablename__ = 'orders'
    id = Column(Integer(), primary_key=True)
    coffee_type = Column(Integer(), ForeignKey('coffee_types.id'), nullable=False)
    user_id = Column(Integer(), ForeignKey('users.id'))
    status = Column(Integer(), nullable=False)
    finishing_time = Column(DateTime())

    user_ordered = relationship("User", foreign_keys=[user_id])
    coffee_ordered = relationship("CoffeeType", foreign_keys=[coffee_type])

    def __init__(self, coffee_type, user_id):
        self.coffee_type = coffee_type
        self.user_id = user_id
        self.status = OrderStatus.PENDING.value
        self.finishing_time = None
